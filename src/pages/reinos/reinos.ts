import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

@Component({
  selector: 'reinos-page',
  templateUrl: 'reinos.html'
})
export class ReinosPage {

  constructor(public navCtrl: NavController) {

  }

}
