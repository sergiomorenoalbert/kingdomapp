import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-marcadores',
  templateUrl: 'marcadores.html'
})
export class MarcadoresPage {

  constructor(public navCtrl: NavController) {

  }

}
