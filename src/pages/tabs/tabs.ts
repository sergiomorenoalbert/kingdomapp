import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { MarcadoresPage } from '../marcadores/marcadores';
import { PerfilPage } from '../perfil/perfil';
import { ReinosPage } from '../reinos/reinos';
@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tab1Root: any = HomePage;
  tab2Root: any = ReinosPage;
  tab3Root: any = MarcadoresPage;
  tab4Root: any = PerfilPage;

  constructor() {

  }
}
