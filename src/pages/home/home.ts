import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { NavController } from 'ionic-angular';
import { HTTP } from 'ionic-native';
import { LoadingController } from 'ionic-angular';
import { PerfilPage } from '../perfil/perfil';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  public result: any  = [];
  public tamanio: any = 0;
  public answers: any = [];

  constructor(
      public navCtrl: NavController,
      public platform: Platform,
      public loading: LoadingController
            ) {
    this.navCtrl = navCtrl;
    platform.ready().then(() => {
      this.ionViewLoaded();
    });
  }
  //Todo en un loader y que llame al get questions
  ionViewLoaded() {
    let loader = this.loading.create({
      content: "Por favor, espera estamos cargando todo...",
      duration: 3000
    });

    loader.present().then(() => {
        this.getQuestions();
    });
  }

  //Cogemos las preguntas
  getQuestions(){
    HTTP.get('https://www.kingdomofwords.com/api/questions', {}, {})
      .then(data => {
        var datos = JSON.parse(data.data);
        this.result = datos;
        this.tamanio = this.result.length;
        this.splitQuestions();
      })
      .catch(error => {
        alert("ERROR "+error)
        console.log(error.status);
        console.log(error.error); // error message as string
        console.log(error.headers);
      });
  }
  splitQuestions(){
    for(var n=0;n<this.tamanio;n++){
      this.answers[n] = this.result[n]['answers'].split("-");
    }
  }

  itemTapped(event, answer) {
    this.navCtrl.push(PerfilPage, {
      answer: answer
    });
  }

}
