import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
//Cada tab de bajo debe ir indicado aqui
import { HomePage } from '../pages/home/home';
import { MarcadoresPage } from '../pages/marcadores/marcadores';
import { PerfilPage } from '../pages/perfil/perfil';
import { ReinosPage } from '../pages/reinos/reinos';
//Tabs de abajo page
import { TabsPage } from '../pages/tabs/tabs';

@NgModule({
  declarations: [
    MyApp,
    ReinosPage,
    PerfilPage,
    HomePage,
    MarcadoresPage,
    TabsPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ReinosPage,
    PerfilPage,
    HomePage,
    MarcadoresPage,
    TabsPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
